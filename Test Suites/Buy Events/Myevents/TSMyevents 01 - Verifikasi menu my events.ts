<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Test suites for TSMyevents 01</description>
   <name>TSMyevents 01 - Verifikasi menu my events</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>6cf9b978-0645-4854-bdf9-0f560835910d</testSuiteGuid>
   <testCaseLink>
      <guid>be5648d6-9975-49cc-8bd8-c0f26d816f5b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Buy Events/TSMyevents 01 - Verifikasi menu my events/TCMyevents 01 - Verifikasi halaman menu my events</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>320ef8db-8949-4c1c-b0b9-f7b90dab15d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Buy Events/TSMyevents 01 - Verifikasi menu my events/TCMyevents 02 - User menekan tombol see all events</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d576a062-b315-486d-aeb1-fcb0deb7e1c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Buy Events/TSMyevents 01 - Verifikasi menu my events/TCMyevents 03 - Pencarian events dengan kata kunci valid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d44c6f6f-9a59-4086-839b-48e04ba5715d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Buy Events/TSMyevents 01 - Verifikasi menu my events/TCMyevents 04 - Pencarian events dengan kata kunci invalid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
