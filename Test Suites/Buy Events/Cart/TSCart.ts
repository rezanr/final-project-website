<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Test suites for cart</description>
   <name>TSCart</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>1be2b561-8981-4a89-b443-05ac56588dc8</testSuiteGuid>
   <testCaseLink>
      <guid>29294248-4384-4992-88cb-74f03a117d7f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Buy Events/TSCart 01 - Verifikasi halaman cart/TCCart 01 - User menambahkan event ke cart</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>6a1a9852-c3ef-41e8-9a17-9801d72be66c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Buy Events/TSCart 01 - Verifikasi halaman cart/TCCart 02 - User menambahkan event yang sama ke cart</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>3fa1b2a7-f48f-4393-80f8-2da09965bec4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Buy Events/TSCart 01 - Verifikasi halaman cart/TCCart 03 - User melihat event lainnya melalui link lihat event lainnya di cart</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
