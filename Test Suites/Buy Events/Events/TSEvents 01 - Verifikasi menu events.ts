<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TSEvents 01 - Verifikasi menu events</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>4569714c-b8bb-42c9-9b51-d674112d12d2</testSuiteGuid>
   <testCaseLink>
      <guid>dcc98e2c-d220-451a-b0dc-b43d457ec8a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PUTRI/TSEvents 01 - Verifikasi menu events/TCEvents 01 - Verifikasi halaman menu events</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0ebf58f4-48f1-48dc-b26a-985ed04e7822</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PUTRI/TSEvents 01 - Verifikasi menu events/TCEvents 02 - User melakukan pencarian events</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f49a3ce5-8815-4deb-90ed-d78753922a77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PUTRI/TSEvents 01 - Verifikasi menu events/TCEvents 03 - User melihat detail events</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4793b8c2-a422-4698-963f-e08dd89f467c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PUTRI/TSEvents 01 - Verifikasi menu events/TCEvents 04 - User menekan tombol muat lebih banyak</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>68984626-cdd0-4e82-b828-8fb5ab5c8c75</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PUTRI/TSEvents 01 - Verifikasi menu events/TCEvents 05 - User berlangganan newslatter</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>722d48d7-8294-41a6-8522-18d0f44af9f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PUTRI/TSEvents 01 - Verifikasi menu events/TCEvents 06 - User melakukan pembelian events sebelum login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
