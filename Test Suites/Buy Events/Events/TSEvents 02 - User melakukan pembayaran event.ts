<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Test suites for TSEvents 02</description>
   <name>TSEvents 02 - User melakukan pembayaran event</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>607188b0-3601-48c8-9dcc-de6c56b9795b</testSuiteGuid>
   <testCaseLink>
      <guid>ebce0519-4fb4-450f-aca3-63c3cfb31760</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Buy Events/TSEvents 02 - User melakukan pembayaran event/TCEvents 07 - User melakukan pembelian events melalui transfer bank</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e488a7d1-0022-4164-a021-b51b24d9f075</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Buy Events/TSEvents 02 - User melakukan pembayaran event/TCEvents 08 - User melakukan pembelian events melalui shoppepay</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b9fbbce6-65bf-4922-a22e-ff21bd211b4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Buy Events/TSEvents 02 - User melakukan pembayaran event/TCEvents 09 - User melakukan pembelian events melalui indomaret</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
