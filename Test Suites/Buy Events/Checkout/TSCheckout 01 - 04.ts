<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Test suites for TSCart 01-04</description>
   <name>TSCheckout 01 - 04</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>3acc1809-c5bf-4c29-963d-797ca2391bd3</testSuiteGuid>
   <testCaseLink>
      <guid>9952b29a-a908-4cbc-a76f-b2d35e247a0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Buy Events/TSCheckout 01 - Verifikasi halaman checkout event/TCCheckout 01 - User menginput data valid pada kode referral</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>461a1296-df6f-442a-9f7f-98cde0c4a15b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Buy Events/TSCheckout 01 - Verifikasi halaman checkout event/TCCheckout 02 - User menginput data invalid pada kode referral</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>bd00a6b2-9710-48b6-863f-84ccdba24081</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Buy Events/TSCheckout 01 - Verifikasi halaman checkout event/TCCheckout 03 - User menginput data null pada kode referral</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9cfa8a1f-2c7f-42f2-80ee-052c537422d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Buy Events/TSCheckout 01 - Verifikasi halaman checkout event/TCCheckout 04 - User input simbol pada kode referral</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
