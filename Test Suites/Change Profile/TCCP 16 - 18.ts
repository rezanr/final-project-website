<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Test suite for Test Case Login</description>
   <name>TCCP 16 - 18</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b7c959ff-a11f-4f04-975d-f7b9ae1b5036</testSuiteGuid>
   <testCaseLink>
      <guid>91957f0d-0e04-4020-a865-192d49a1bfa0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 16 - User mengubah tanggal lahir</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c2656442-f141-460f-9d79-05b6c8668876</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 17 - User tidak bisa mengosongkan tanggal lahir</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a592ac4e-a9d7-45f3-875a-98dad77bf94b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 18 - User tidak bisa mengubah email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
