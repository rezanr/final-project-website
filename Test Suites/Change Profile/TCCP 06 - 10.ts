<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Test suite for Test Case</description>
   <name>TCCP 06 - 10</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>9b3dd301-15bd-4465-b33e-fa435c50dfa6</testSuiteGuid>
   <testCaseLink>
      <guid>51949e28-9595-4fbe-a526-613de2f72106</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 06 - User input nama null</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>12152d8a-d646-443f-841b-b6c4280c2575</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 07 - User mengganti nama memakai alpabet dan angka</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9c1a4414-0397-4f5c-aa8e-4aff36eda6e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 08 - User mengganti nama memakai alphabet dan karakter</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>7ffdbca3-a76b-4324-a675-61bac5c0cef5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 09 - User mengubah nomor phone</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f264ced7-308e-4ea7-aea0-2fe75d421c66</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 10 - User mengubah nomor phone kurang dari 10 angka</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
