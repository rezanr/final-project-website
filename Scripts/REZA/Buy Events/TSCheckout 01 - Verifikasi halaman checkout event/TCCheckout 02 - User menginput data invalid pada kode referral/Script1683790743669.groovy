import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('REZA/Buy Events/Login Events'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('REZA/Buy Events/TSCheckout 01 - Verifikasi halaman checkout event/TCCheckout 02 - User menginput data invalid pada kode referral/toggle_akun'))

WebUI.click(findTestObject('REZA/Buy Events/TSCheckout 01 - Verifikasi halaman checkout event/TCCheckout 02 - User menginput data invalid pada kode referral/menu_Checkout'))

WebUI.takeFullPageScreenshot()

WebUI.check(findTestObject('REZA/Buy Events/TSCheckout 01 - Verifikasi halaman checkout event/TCCheckout 02 - User menginput data invalid pada kode referral/input_cekbox'))

WebUI.setText(findTestObject('REZA/Buy Events/TSCheckout 01 - Verifikasi halaman checkout event/TCCheckout 02 - User menginput data invalid pada kode referral/input_Checkout_referral_code'), 
    'P52AAA')

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('REZA/Buy Events/TSCheckout 01 - Verifikasi halaman checkout event/TCCheckout 02 - User menginput data invalid pada kode referral/button_Apply'))

WebUI.verifyElementText(findTestObject('REZA/Buy Events/TSCheckout 01 - Verifikasi halaman checkout event/TCCheckout 02 - User menginput data invalid pada kode referral/alert_Referal or Voucher Not Found'), 
    'Referal or Voucher Not Found')

WebUI.takeFullPageScreenshot()

