import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('REZA/Buy Events/Login Events'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('REZA/Buy Events/TSEvents 02 - User melakukan pembayaran event/TCEvents 08 - User melakukan pembelian events melalui shoppepay/menu_Events'))

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('REZA/Buy Events/TSEvents 02 - User melakukan pembayaran event/TCEvents 08 - User melakukan pembelian events melalui shoppepay/day 4 workshop'))

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('REZA/Buy Events/TSEvents 02 - User melakukan pembayaran event/TCEvents 08 - User melakukan pembelian events melalui shoppepay/a_Beli Tiket'))

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('REZA/Buy Events/TSEvents 02 - User melakukan pembayaran event/TCEvents 08 - User melakukan pembelian events melalui shoppepay/a_Lihat Pembelian Saya'))

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('REZA/Buy Events/TSEvents 02 - User melakukan pembayaran event/TCEvents 08 - User melakukan pembelian events melalui shoppepay/button_Checkout'))

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('REZA/Buy Events/TSEvents 02 - User melakukan pembayaran event/TCEvents 08 - User melakukan pembelian events melalui shoppepay/input_Total Pembayaran_payment_method'))

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('REZA/Buy Events/TSEvents 02 - User melakukan pembayaran event/TCEvents 08 - User melakukan pembelian events melalui shoppepay/button_Confirm'))

WebUI.delay(10)

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('REZA/Buy Events/TSEvents 02 - User melakukan pembayaran event/TCEvents 08 - User melakukan pembelian events melalui shoppepay/a_ShopeePay'))

WebUI.delay(5)

WebUI.takeFullPageScreenshot()

