import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('REZA/Buy Events/Login Events'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('REZA/Buy Events/TSCart 01 - Verifikasi halaman cart/TCCart 01 - User menambahkan event ke cart/menu_Events'))

WebUI.click(findTestObject('REZA/Buy Events/TSCart 01 - Verifikasi halaman cart/TCCart 01 - User menambahkan event ke cart/div_Day 3 Predict using Machine Learning'))

WebUI.takeFullPageScreenshot([])

WebUI.click(findTestObject('Buy Events/TSCart 01 - Verifikasi halaman cart/TCCart 01 - User menambahkan event ke cart/btn_Beli Tiket'))

WebUI.verifyElementText(findTestObject('Object Repository/REZA/Buy Events/TSCart 01 - Verifikasi halaman cart/TCCart 01 - User menambahkan event ke cart/h3_add to cart success'), 
    'add to cart success')

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('Object Repository/REZA/Buy Events/TSCart 01 - Verifikasi halaman cart/TCCart 01 - User menambahkan event ke cart/btn_Lihat Pembelian Saya'))

WebUI.takeFullPageScreenshot()

