import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

// Mengirim permintaan login API
def response = WS.sendRequest(findTestObject('Login API'))

// Mendapatkan status code dari respons
def statusCode = response.getStatusCode()

// Memverifikasi status code
assert statusCode == 200 : "Status Code tidak sesuai, seharusnya 200 tetapi ditemukan ${statusCode}"

// Menampilkan status code
println("Status Code: " + statusCode)
