import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/daftar?')

WebUI.setText(findTestObject('BOBY/REGISTER/Nama'), 'Boby')

WebUI.setText(findTestObject('BOBY/REGISTER/Tanggal lahir'), '15-May-2002')

WebUI.setText(findTestObject('BOBY/REGISTER/E-Mail'), 'Boby@gmail.com')

WebUI.setText(findTestObject('BOBY/REGISTER/Whatsapp'), '085358000000')

WebUI.setEncryptedText(findTestObject('BOBY/REGISTER/Kata Sandi'), 'BR4+6ebIRw/89ruX5hJDag==')

WebUI.setEncryptedText(findTestObject('BOBY/REGISTER/Konfirmasi kata sandi'), 'BR4+6ebIRw/89ruX5hJDag==')

WebUI.click(findTestObject('BOBY/REGISTER/Checkbox1'))

WebUI.click(findTestObject('BOBY/REGISTER/button_Daftar'))

