import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://demo-app.online/')

WebUI.click(findTestObject('Object Repository/PUTRI/TSEvents 01 - Verifikasi menu events/TCEvents 05 - User berlangganan newslatter/menu_Events'))

WebUI.setText(findTestObject('Object Repository/PUTRI/TSEvents 01 - Verifikasi menu events/TCEvents 05 - User berlangganan newslatter/input_Gabung CODING.ID Newsletter_email'), 
    'kabutominasan@gmail.com')

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('Object Repository/PUTRI/TSEvents 01 - Verifikasi menu events/TCEvents 05 - User berlangganan newslatter/button_Berlangganan'))

WebUI.takeFullPageScreenshot()

WebUI.verifyElementText(findTestObject('Object Repository/PUTRI/TSEvents 01 - Verifikasi menu events/TCEvents 05 - User berlangganan newslatter/p_Email Already taken'), 
    'Email Already taken.')

WebUI.click(findTestObject('Object Repository/PUTRI/TSEvents 01 - Verifikasi menu events/TCEvents 05 - User berlangganan newslatter/button_Close'))

WebUI.closeBrowser()

