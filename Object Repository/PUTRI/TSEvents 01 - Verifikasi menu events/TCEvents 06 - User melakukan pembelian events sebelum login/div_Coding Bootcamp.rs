<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Coding Bootcamp</name>
   <tag></tag>
   <elementGuidId>e42ab9ba-5f5d-43e1-8a72-bd9d646aeb70</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.containerHeader</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ebb39537-4b5c-47b0-8c0e-8da706939d40</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>containerHeader</value>
      <webElementGuid>8f9e8b32-6ec9-45ca-ad20-e967eec95b26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-data</name>
      <type>Main</type>
      <value>{}</value>
      <webElementGuid>c9eac47f-7efa-4e08-a4e8-bb1a850791e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Coding Bootcamp
            Tech Talent Berkualitas

        Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.
                    
                
                    
                    
                        Quality Assurance Engineer Class
                                                    
                                Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                    
                
                    
                    
                        Fullstack Engineer Class
                                                    
                                Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                



        
            
                
                    Belum kerja?
                        Tidak usah bayar!
                    Jaminan uang kembali 100% jika kamu tidak
                        mendapatkan
                        pekerjaan untuk Upfront Payment dan tidak perlu
                        membayar sampai kamu dapat kerja dengan metode ISA
                

                
                    Belajar dari Praktisi
                    Coach di Coding.ID merupakan praktisi yang telah
                        berpengalaman di bidangnya loh jadi tenang aja kamu berada di tangan yang tepat kok
                

                
                    Akses ke semua
                        Event &amp; Course
                    Peserta maupun alumni akan mendapatkan privilage
                        untuk
                        mengakses semua semua Events maupun Online Course di Coding.ID secara gratis!
                

                
                    Belajar dari Nol sampai Jago
                    Kamu masih awam? Tenang aja Coding.ID akan
                        membangun
                        kemampuan programing kamu secara mandiri dengan cara-cara yang sangat sederhana.
                
            


            
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            

        

    </value>
      <webElementGuid>27a5f003-1a28-4123-a867-6566b9835c2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;containerHeader&quot;]</value>
      <webElementGuid>0df88826-4ee8-468d-8feb-6ce4e8397e5a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div</value>
      <webElementGuid>723affc0-3ae7-4ea5-8b5f-c362300280af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[4]/div</value>
      <webElementGuid>be50e9cc-88b4-4ba1-8d20-6c091f4a98f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Coding Bootcamp
            Tech Talent Berkualitas

        Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.
                    
                
                    
                    
                        Quality Assurance Engineer Class
                                                    
                                Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                    
                
                    
                    
                        Fullstack Engineer Class
                                                    
                                Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                



        
            
                
                    Belum kerja?
                        Tidak usah bayar!
                    Jaminan uang kembali 100% jika kamu tidak
                        mendapatkan
                        pekerjaan untuk Upfront Payment dan tidak perlu
                        membayar sampai kamu dapat kerja dengan metode ISA
                

                
                    Belajar dari Praktisi
                    Coach di Coding.ID merupakan praktisi yang telah
                        berpengalaman di bidangnya loh jadi tenang aja kamu berada di tangan yang tepat kok
                

                
                    Akses ke semua
                        Event &amp; Course
                    Peserta maupun alumni akan mendapatkan privilage
                        untuk
                        mengakses semua semua Events maupun Online Course di Coding.ID secara gratis!
                

                
                    Belajar dari Nol sampai Jago
                    Kamu masih awam? Tenang aja Coding.ID akan
                        membangun
                        kemampuan programing kamu secara mandiri dengan cara-cara yang sangat sederhana.
                
            


            
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            

        

    ' or . = '
        Coding Bootcamp
            Tech Talent Berkualitas

        Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.
                    
                
                    
                    
                        Quality Assurance Engineer Class
                                                    
                                Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                    
                
                    
                    
                        Fullstack Engineer Class
                                                    
                                Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                



        
            
                
                    Belum kerja?
                        Tidak usah bayar!
                    Jaminan uang kembali 100% jika kamu tidak
                        mendapatkan
                        pekerjaan untuk Upfront Payment dan tidak perlu
                        membayar sampai kamu dapat kerja dengan metode ISA
                

                
                    Belajar dari Praktisi
                    Coach di Coding.ID merupakan praktisi yang telah
                        berpengalaman di bidangnya loh jadi tenang aja kamu berada di tangan yang tepat kok
                

                
                    Akses ke semua
                        Event &amp; Course
                    Peserta maupun alumni akan mendapatkan privilage
                        untuk
                        mengakses semua semua Events maupun Online Course di Coding.ID secara gratis!
                

                
                    Belajar dari Nol sampai Jago
                    Kamu masih awam? Tenang aja Coding.ID akan
                        membangun
                        kemampuan programing kamu secara mandiri dengan cara-cara yang sangat sederhana.
                
            


            
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            

        

    ')]</value>
      <webElementGuid>373e2996-d8a3-4263-9caa-f8e16d2368fd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
