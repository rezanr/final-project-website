<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Day 4 Workshop</name>
   <tag></tag>
   <elementGuidId>0480cb27-1249-4695-a430-a97590a051d9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.cardOuter</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='blockListEvent']/a/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>0ac8e319-613f-4f66-9ca3-7cd26b805b8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cardOuter</value>
      <webElementGuid>4fda2f73-43a0-44e0-bea9-26ab3d98c8d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                    
                                        
                                                                                    
                                            
                                            
                                                                            

                                    
                                        
                                            Day 4: Workshop
                                            
                                        
                                        
                                            With Ziyad Syauqi Fawwazi
                                            
                                        
                                    

                                
                                
                                                                            OPEN until
                                                25 Nov 2023 11:30 WIB
                                        
                                                                        Mini Class
                                    
                                    
                                        Day 4: Workshop
                                        
                                    
                                    
                                        25 Nov 2023 |
                                        19:30 WIB Via
                                        Zoom
                                        
                                    
                                                                                                                        Rp.
                                                    500.000

                                                                                                    Rp
                                                        85.000
                                                                                            
                                                                                                            
                            </value>
      <webElementGuid>e78e131b-a108-4be3-81ea-3bbc35d69272</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[@class=&quot;wm-sticky&quot;]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;wm-courses wm-courses-popular&quot;]/ul[@class=&quot;row&quot;]/li[@id=&quot;blockListEvent&quot;]/a[1]/div[@class=&quot;cardOuter&quot;]</value>
      <webElementGuid>f7db6987-4fe8-472c-bcd7-54268910c3fe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='blockListEvent']/a/div</value>
      <webElementGuid>01f1c993-4ea8-49fa-a418-81c912bb2b64</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Event lain yang banyak diminati'])[1]/following::div[2]</value>
      <webElementGuid>0f2c1f86-197e-46b8-b1f7-775e9c0140a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Beli Tiket'])[1]/following::div[3]</value>
      <webElementGuid>dd212788-53a6-4114-aeab-e1b9a9fb94e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div</value>
      <webElementGuid>e5a6f8b5-1273-46df-9d05-922793767f5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                
                                    
                                        
                                                                                    
                                            
                                            
                                                                            

                                    
                                        
                                            Day 4: Workshop
                                            
                                        
                                        
                                            With Ziyad Syauqi Fawwazi
                                            
                                        
                                    

                                
                                
                                                                            OPEN until
                                                25 Nov 2023 11:30 WIB
                                        
                                                                        Mini Class
                                    
                                    
                                        Day 4: Workshop
                                        
                                    
                                    
                                        25 Nov 2023 |
                                        19:30 WIB Via
                                        Zoom
                                        
                                    
                                                                                                                        Rp.
                                                    500.000

                                                                                                    Rp
                                                        85.000
                                                                                            
                                                                                                            
                            ' or . = '
                                
                                    
                                        
                                                                                    
                                            
                                            
                                                                            

                                    
                                        
                                            Day 4: Workshop
                                            
                                        
                                        
                                            With Ziyad Syauqi Fawwazi
                                            
                                        
                                    

                                
                                
                                                                            OPEN until
                                                25 Nov 2023 11:30 WIB
                                        
                                                                        Mini Class
                                    
                                    
                                        Day 4: Workshop
                                        
                                    
                                    
                                        25 Nov 2023 |
                                        19:30 WIB Via
                                        Zoom
                                        
                                    
                                                                                                                        Rp.
                                                    500.000

                                                                                                    Rp
                                                        85.000
                                                                                            
                                                                                                            
                            ')]</value>
      <webElementGuid>fa0a3a88-d5d5-4247-8bf8-4b24118581d4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
