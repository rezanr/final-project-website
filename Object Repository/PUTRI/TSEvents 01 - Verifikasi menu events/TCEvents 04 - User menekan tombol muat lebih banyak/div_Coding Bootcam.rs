<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Coding Bootcam</name>
   <tag></tag>
   <elementGuidId>d84171f2-7ebe-4a2e-9ef3-0a8148c9d43d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.containerHeader</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>0a2e4644-554e-4b36-9d94-33a86ccbca7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>containerHeader</value>
      <webElementGuid>e4294cab-ceb3-42bc-8e45-1525d45034b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-data</name>
      <type>Main</type>
      <value>{}</value>
      <webElementGuid>2d5c4465-3691-4221-a278-466ad9f52fb6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Coding Bootcamp
            Tech Talent Berkualitas

        Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.
                    
                
                    
                    
                        Quality Assurance Engineer Class
                                                    
                                Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                    
                
                    
                    
                        Fullstack Engineer Class
                                                    
                                Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                



        
            
                
                    Belum kerja?
                        Tidak usah bayar!
                    Jaminan uang kembali 100% jika kamu tidak
                        mendapatkan
                        pekerjaan untuk Upfront Payment dan tidak perlu
                        membayar sampai kamu dapat kerja dengan metode ISA
                

                
                    Belajar dari Praktisi
                    Coach di Coding.ID merupakan praktisi yang telah
                        berpengalaman di bidangnya loh jadi tenang aja kamu berada di tangan yang tepat kok
                

                
                    Akses ke semua
                        Event &amp; Course
                    Peserta maupun alumni akan mendapatkan privilage
                        untuk
                        mengakses semua semua Events maupun Online Course di Coding.ID secara gratis!
                

                
                    Belajar dari Nol sampai Jago
                    Kamu masih awam? Tenang aja Coding.ID akan
                        membangun
                        kemampuan programing kamu secara mandiri dengan cara-cara yang sangat sederhana.
                
            


            
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            

        

    </value>
      <webElementGuid>3a7c38cd-de82-4ae8-9f7c-f8e788197acf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;containerHeader&quot;]</value>
      <webElementGuid>f5a49288-df7d-4d8b-872c-44ec1ede58a2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div</value>
      <webElementGuid>f35006cf-e410-4530-ae7a-9f7a479a07bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[4]/div</value>
      <webElementGuid>1b9d8448-6b17-40f3-a483-e9684998bebd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Coding Bootcamp
            Tech Talent Berkualitas

        Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.
                    
                
                    
                    
                        Quality Assurance Engineer Class
                                                    
                                Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                    
                
                    
                    
                        Fullstack Engineer Class
                                                    
                                Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                



        
            
                
                    Belum kerja?
                        Tidak usah bayar!
                    Jaminan uang kembali 100% jika kamu tidak
                        mendapatkan
                        pekerjaan untuk Upfront Payment dan tidak perlu
                        membayar sampai kamu dapat kerja dengan metode ISA
                

                
                    Belajar dari Praktisi
                    Coach di Coding.ID merupakan praktisi yang telah
                        berpengalaman di bidangnya loh jadi tenang aja kamu berada di tangan yang tepat kok
                

                
                    Akses ke semua
                        Event &amp; Course
                    Peserta maupun alumni akan mendapatkan privilage
                        untuk
                        mengakses semua semua Events maupun Online Course di Coding.ID secara gratis!
                

                
                    Belajar dari Nol sampai Jago
                    Kamu masih awam? Tenang aja Coding.ID akan
                        membangun
                        kemampuan programing kamu secara mandiri dengan cara-cara yang sangat sederhana.
                
            


            
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            

        

    ' or . = '
        Coding Bootcamp
            Tech Talent Berkualitas

        Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.
                    
                
                    
                    
                        Quality Assurance Engineer Class
                                                    
                                Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                    
                
                    
                    
                        Fullstack Engineer Class
                                                    
                                Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                



        
            
                
                    Belum kerja?
                        Tidak usah bayar!
                    Jaminan uang kembali 100% jika kamu tidak
                        mendapatkan
                        pekerjaan untuk Upfront Payment dan tidak perlu
                        membayar sampai kamu dapat kerja dengan metode ISA
                

                
                    Belajar dari Praktisi
                    Coach di Coding.ID merupakan praktisi yang telah
                        berpengalaman di bidangnya loh jadi tenang aja kamu berada di tangan yang tepat kok
                

                
                    Akses ke semua
                        Event &amp; Course
                    Peserta maupun alumni akan mendapatkan privilage
                        untuk
                        mengakses semua semua Events maupun Online Course di Coding.ID secara gratis!
                

                
                    Belajar dari Nol sampai Jago
                    Kamu masih awam? Tenang aja Coding.ID akan
                        membangun
                        kemampuan programing kamu secara mandiri dengan cara-cara yang sangat sederhana.
                
            


            
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            

        

    ')]</value>
      <webElementGuid>14eb89a1-ddff-4c1d-a0bc-70fa1e483cf0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
