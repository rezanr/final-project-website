<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Transfer QR Code  Bank (cek otomatis)</name>
   <tag></tag>
   <elementGuidId>dad4914a-f916-40cc-8f29-4dc99d0c8229</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>td.product-col</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='modalform']/table[2]/tbody/tr/td[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>ac3e8698-3c6f-4255-b7ef-f82bd36199de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-col</value>
      <webElementGuid>6367b222-75d1-4b53-ab53-94691a61b403</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            Transfer QR Code / Bank (cek otomatis)
                                        </value>
      <webElementGuid>358ba3db-53fc-44e9-98cd-5f960b4ddbce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;modalform&quot;)/table[2]/tbody[1]/tr[1]/td[@class=&quot;product-col&quot;]</value>
      <webElementGuid>05d34cc8-4dba-40eb-a424-a48b7db83c17</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='modalform']/table[2]/tbody/tr/td[2]</value>
      <webElementGuid>db19318b-b33e-438c-9764-7b7b159f74db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total Pembayaran'])[1]/following::td[3]</value>
      <webElementGuid>ac37e88f-5fc1-4785-90a2-b89a5778ae9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-'])[2]/following::td[4]</value>
      <webElementGuid>32fc22c9-7beb-4be0-a547-d7f3804ad051</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm'])[1]/preceding::td[5]</value>
      <webElementGuid>d7561ebe-8dbf-4964-9e93-76aaf3ab4aa9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Transfer QR Code / Bank (cek otomatis)']/parent::*</value>
      <webElementGuid>07e0dede-2849-4eb9-9004-79695834a796</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//table[2]/tbody/tr/td[2]</value>
      <webElementGuid>ac90e440-5fef-486f-868c-0756e8966bc1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '
                                            Transfer QR Code / Bank (cek otomatis)
                                        ' or . = '
                                            Transfer QR Code / Bank (cek otomatis)
                                        ')]</value>
      <webElementGuid>2e877f4f-eda8-4486-995a-afd1159e64b1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
