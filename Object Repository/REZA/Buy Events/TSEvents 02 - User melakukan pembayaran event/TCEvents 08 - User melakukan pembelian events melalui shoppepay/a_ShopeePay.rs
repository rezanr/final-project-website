<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_ShopeePay</name>
   <tag></tag>
   <elementGuidId>e83125f7-df42-4b20-8a6c-301013e7d5e1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[4]/a[@class=&quot;list&quot;][count(. | //a[@class = 'list' and @data-testid = 'list-item' and @href = '#/shopeepay-qris' and (text() = 'ShopeePay' or . = 'ShopeePay') and @ref_element = 'Object Repository/Buy Events/TSEvents 02 - User melakukan pembayaran event/TCEvents 08 - User melakukan pembelian events melalui shoppepay/iframe_concat(id(, , snap-midtrans, , ))_po_e8a031']) = count(//a[@class = 'list' and @data-testid = 'list-item' and @href = '#/shopeepay-qris' and (text() = 'ShopeePay' or . = 'ShopeePay') and @ref_element = 'Object Repository/Buy Events/TSEvents 02 - User melakukan pembayaran event/TCEvents 08 - User melakukan pembelian events melalui shoppepay/iframe_concat(id(, , snap-midtrans, , ))_po_e8a031'])]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div[4]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>86160c84-df20-4354-abce-2463f8b001b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list</value>
      <webElementGuid>e4cd2429-dc92-4290-8d2a-9fca53b07200</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>list-item</value>
      <webElementGuid>af1dd827-d967-422f-81a7-c7d5e004a50b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#/shopeepay-qris</value>
      <webElementGuid>c0383789-dc19-4495-aa04-d8cbc3fa94b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>ShopeePay</value>
      <webElementGuid>9c12d216-fc18-4636-973c-91c8bab544a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[4]/a[@class=&quot;list&quot;]</value>
      <webElementGuid>03aac631-aca6-4f36-bd46-c8830c5d5102</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/REZA/Buy Events/TSEvents 02 - User melakukan pembayaran event/TCEvents 08 - User melakukan pembelian events melalui shoppepay/iframe_concat(id(, , snap-midtrans, , ))_po_e8a031</value>
      <webElementGuid>27bfa88b-1cb6-4929-90cf-cd78b832902c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[4]/a</value>
      <webElementGuid>74ff76c7-7d37-46b8-aa56-cbefacae9f52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bank transfer'])[1]/following::a[1]</value>
      <webElementGuid>16a29cd0-36a6-4aea-9841-a3f27f75ed1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Credit/debit card'])[1]/following::a[2]</value>
      <webElementGuid>45a01d1c-6470-43d7-9205-aa8519be58d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BCA KlikPay'])[1]/preceding::a[1]</value>
      <webElementGuid>4b56fcac-fb5e-43a6-aeed-62cb9ac96aae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '#/shopeepay-qris')]</value>
      <webElementGuid>1fe49262-32c2-4c2c-b797-05396ffcf8e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/a</value>
      <webElementGuid>5ed3d197-abd2-4599-b3be-82cf9b02d887</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#/shopeepay-qris' and (text() = 'ShopeePay' or . = 'ShopeePay')]</value>
      <webElementGuid>0c9457b3-daf9-4875-aead-6e9fe4291b42</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
