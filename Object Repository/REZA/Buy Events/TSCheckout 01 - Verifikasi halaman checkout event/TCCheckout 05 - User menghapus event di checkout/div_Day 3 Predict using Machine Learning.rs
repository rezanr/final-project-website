<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Day 3 Predict using Machine Learning</name>
   <tag></tag>
   <elementGuidId>f5d66215-e937-4d06-bbf8-0d4ef99d2e0c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.cardOuter</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='blockListEvent']/a/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>aa767b4f-d3c0-4cdd-b673-e77b2e3d8893</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cardOuter</value>
      <webElementGuid>e2a1d3f3-2ac5-4af9-815f-9e9c8343d9b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 3: Predict using Machine Learning
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        18 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 3: Predict using Machine Learning
                                                
                                            
                                            
                                                18 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    </value>
      <webElementGuid>fcf0977e-b8f6-41a2-bc21-5208cf5eb6d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loopevent&quot;)/li[@id=&quot;blockListEvent&quot;]/a[1]/div[@class=&quot;cardOuter&quot;]</value>
      <webElementGuid>f03de002-7353-46e3-846f-0040b5af77cd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='blockListEvent']/a/div</value>
      <webElementGuid>d5ed2319-d62c-4f61-b024-8b99e4fd11ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Events'])[4]/following::div[5]</value>
      <webElementGuid>675a638a-e1a5-40f7-80d7-c515979ee42c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CODING.ID Event'])[1]/following::div[10]</value>
      <webElementGuid>24f69479-b9c8-4569-becb-63f2ebed4450</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div</value>
      <webElementGuid>97fd24ee-bbe3-4c28-8ba6-667455c30e9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 3: Predict using Machine Learning
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        18 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 3: Predict using Machine Learning
                                                
                                            
                                            
                                                18 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    ' or . = '
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 3: Predict using Machine Learning
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        18 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 3: Predict using Machine Learning
                                                
                                            
                                            
                                                18 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    ')]</value>
      <webElementGuid>4d58d429-0312-4aa3-8e35-545527e92d8d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
