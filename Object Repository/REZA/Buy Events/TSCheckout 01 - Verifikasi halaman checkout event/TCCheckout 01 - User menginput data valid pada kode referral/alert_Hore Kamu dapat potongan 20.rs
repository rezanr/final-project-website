<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alert_Hore Kamu dapat potongan 20</name>
   <tag></tag>
   <elementGuidId>398092a4-ed42-4ceb-a011-e23f9a7ecfea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'success_message' and (text() = 'Hore! Kamu dapat potongan 20% ' or . = 'Hore! Kamu dapat potongan 20% ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#success_message</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//small[@id='success_message']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>small</value>
      <webElementGuid>7b03e84f-99b7-4541-986a-d79e5db10da3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>success_message</value>
      <webElementGuid>d0ece29b-28d9-46be-bb71-ba766b8cb4c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Hore! Kamu dapat potongan 20% </value>
      <webElementGuid>a49fb166-8a76-43d1-bd17-eaf82d745d5b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;success_message&quot;)</value>
      <webElementGuid>b8e24559-8b6e-4f92-af09-cc8b995ebe88</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//small[@id='success_message']</value>
      <webElementGuid>d66c60cf-b199-43b1-92dd-0608ed5fa19c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='cartForm']/div[2]/small[2]</value>
      <webElementGuid>6c8d0f3d-20cf-4889-942d-3cd0d995e332</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply'])[1]/following::small[2]</value>
      <webElementGuid>de8fe9da-1b08-4099-abd6-e0f0516d8b3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Checkout'])[4]/following::small[2]</value>
      <webElementGuid>9d36c8b6-22d4-415a-a37c-9b9225b4e627</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Riwayat Pembelian'])[1]/preceding::small[1]</value>
      <webElementGuid>3d12026a-0bbc-488a-bc97-6d7deb9e1ecc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EVN06042300001'])[1]/preceding::small[1]</value>
      <webElementGuid>8c58d039-560d-471a-8169-587190e8001e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Hore! Kamu dapat potongan 20%']/parent::*</value>
      <webElementGuid>b8b6d7b4-ec11-40c3-9619-26400e31c4ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//small[2]</value>
      <webElementGuid>bddc15bd-c649-45b6-ba67-ed9adbb304bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//small[@id = 'success_message' and (text() = 'Hore! Kamu dapat potongan 20% ' or . = 'Hore! Kamu dapat potongan 20% ')]</value>
      <webElementGuid>ce177c14-b27c-4115-b7fa-030488b08f27</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
