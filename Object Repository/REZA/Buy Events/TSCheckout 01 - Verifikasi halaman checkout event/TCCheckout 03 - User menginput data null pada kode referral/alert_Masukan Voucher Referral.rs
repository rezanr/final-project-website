<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alert_Masukan Voucher Referral</name>
   <tag></tag>
   <elementGuidId>d78ae7f2-c3d7-453a-9ccf-8ae9697a4c80</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'error_message' and (text() = 'Masukan Voucher / Referral' or . = 'Masukan Voucher / Referral')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#error_message</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//small[@id='error_message']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>small</value>
      <webElementGuid>ba120d7b-9abe-440d-8b70-f6ba865f435d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>error_message</value>
      <webElementGuid>779a55e8-162c-4fb5-b764-bea2bc01dff3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Masukan Voucher / Referral</value>
      <webElementGuid>0871df9a-9b59-42e4-9e73-a5231441a313</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;error_message&quot;)</value>
      <webElementGuid>3c06d8bf-8071-4b40-876e-c3becb893d69</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//small[@id='error_message']</value>
      <webElementGuid>65501442-860f-4be2-8b25-1311f6ddf534</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='cartForm']/div[2]/small</value>
      <webElementGuid>e23974c4-d3fe-4e56-a180-295ffd34e147</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply'])[1]/following::small[1]</value>
      <webElementGuid>da03ee10-4134-4228-adeb-839765f9531d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Checkout'])[4]/following::small[1]</value>
      <webElementGuid>13cccf4f-5afc-4ee0-9725-b2c0ab31801b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Riwayat Pembelian'])[1]/preceding::small[2]</value>
      <webElementGuid>76709416-4187-4629-ab27-f91a28a27c06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EVN06042300001'])[1]/preceding::small[2]</value>
      <webElementGuid>e947d74f-9ecd-4057-a66b-5f40b6b90d1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Masukan Voucher / Referral']/parent::*</value>
      <webElementGuid>a376317a-9b4e-42c9-9f17-70f9449d1fd1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//small</value>
      <webElementGuid>c64b4bff-1fea-478c-bd93-400592876722</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//small[@id = 'error_message' and (text() = 'Masukan Voucher / Referral' or . = 'Masukan Voucher / Referral')]</value>
      <webElementGuid>eaffcfb2-d994-4082-8434-051cf53a27ee</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
